import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Modal } from "../framework/Modal/Modal";
import { closeModal } from "../store/actions/app.action";

import { ErrorModal, SuccessModal } from "./Modals";

export const SUCCESS_MODAL = "SUCCESS_MODAL";
export const ERROR_MODAL = "ERROR_MODAL";

class ModalWrapper extends Component {
  constructor(props) {
    super(props);
    this.hideModal = this.hideModal.bind(this);
  }

  componentDidUpdate() {
    if (this.props.modal) this.modal.show();
  }

  hideModal() {
    setTimeout(() => {
      this.props.closeModal(null);
    }, 400);
  }

  renderModal() {
    switch (this.props.modal) {
      case ERROR_MODAL:
        return <ErrorModal modal={this.modal} />;

      case SUCCESS_MODAL:
        return <SuccessModal modal={this.modal} />;

      default:
    }
  }

  render() {
    const { config } = this.props;

    return (
      <Modal
        onMount={(ref) => (this.modal = ref)}
        nonDismissable={config.nonDismissable}
        onHide={this.hideModal}
        forceNormalLayout
      >
        {this.renderModal()}
      </Modal>
    );
  }
}

function mapStatetoProps({ app }) {
  return {
    modal: app.modal.current,
    config: app.modal.config,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ closeModal }, dispatch);
}

export default connect(mapStatetoProps, mapDispatchToProps)(ModalWrapper);
