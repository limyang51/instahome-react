import ErrorModal from "./Error.modal";
import SuccessModal from "./Success.modal";

export { ErrorModal, SuccessModal };
