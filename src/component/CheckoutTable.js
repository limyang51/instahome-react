import React, { Component } from "react";
import { Table, H3, Flex, Button } from "../framework";

class CheckoutTable extends Component {
  columns() {
    return [
      {
        Header: "No",
        Cell: ({ index }) => {
          return <div>{index + 1}</div>;
        },
        Footer: "Total Balance:",
      },
      {
        Header: "Ad",
        accessor: "ad",
      },
      {
        Header: "Quantity",
        accessor: "quantity",
      },
      {
        Header: "Price (Per Unit)",
        accessor: "price",
      },
      {
        Header: "Discount",
        accessor: "discount",
      },
      {
        Header: "Total",
        accessor: "totalPrice",
        Footer: (info) => {
          return (
            <span>
              {info.data
                .reduce((total, { totalPrice }) => (total += totalPrice), 0)
                .toFixed(2)}
            </span>
          );
        },
      },
    ];
  }

  render() {
    return (
      <div>
        {this.props.bills.length > 0 &&
          this.props.bills.map((bill, idx) => (
            <Flex key={bill.customer} column>
              <Flex>
                <H3>{bill.customer}</H3>
                <Button
                  color="success"
                  onClick={() => this.props.onCalculate(bill)}
                >
                  Calculate
                </Button>
              </Flex>
              <Table
                data={bill.ads}
                centeredContent
                disableNextButton={true}
                currentPage={1}
                defaultPageSize={10}
                showPageSizeOptions={false}
                showNavigation={false}
                minRows={10}
                columns={this.columns()}
              />
            </Flex>
          ))}
      </div>
    );
  }
}

export default CheckoutTable;
