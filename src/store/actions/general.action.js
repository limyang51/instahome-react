import {
  SET_LIST_CUSTOMERS,
  SET_LIST_ADS,
  SET_CHECKOUT_BILL,
  ADD_NEW_ITEM_CART,
  APPEND_ITEM_CART,
} from "../reducers/general.reducer";
import * as generalApi from "../services/general.api";

export function listCustomer() {
  return async (dispatch) => {
    const { customers } = await generalApi.listCustomers();
    dispatch({
      type: SET_LIST_CUSTOMERS,
      payload: { customers },
    });
  };
}

export function listAds() {
  return async (dispatch) => {
    const { adsType } = await generalApi.listAdType();
    dispatch({
      type: SET_LIST_ADS,
      payload: { adsType },
    });
  };
}

export function addItem(item) {
  return async (dispatch, getState) => {
    const { bills, ads } = getState().general;
    const matchBillIndex = bills.findIndex(
      (bill) => bill.customer === item.customer
    );

    if (matchBillIndex > -1) {
      const matchAdIndex = bills[matchBillIndex].ads.findIndex(
        ({ ad }) => ad === item.ad
      );
      if (matchAdIndex > -1) {
        bills[matchBillIndex].ads[matchAdIndex].quantity =
          Number(bills[matchBillIndex].ads[matchAdIndex].quantity) +
          Number(item.quantity);
      } else {
        const newAdd = { ad: item.ad, quantity: item.quantity };

        bills[matchBillIndex].ads.push(newAdd);
      }
      dispatch({
        type: APPEND_ITEM_CART,
        payload: { bills: bills },
      });
    } else {
      const { price } = ads.find((ad) => ad.adType === item.ad);

      dispatch({
        type: ADD_NEW_ITEM_CART,
        payload: {
          item: {
            ...item,
            price,
          },
        },
      });
    }
  };
}

export function calculatePrice({ customer, ads }) {
  return async (dispatch, getState) => {
    const result = await generalApi.chcekout({ customer, ads });

    const { bills } = getState().general;
    const matchBillIndex = bills.findIndex(
      (bill) => bill.customer === customer
    );
    if (matchBillIndex > -1) {
      bills[matchBillIndex] = result;
    }
    dispatch({
      type: SET_CHECKOUT_BILL,
      payload: { bills: bills },
    });
  };
}
