import { OPEN_MODAL, CLOSE_MODAL } from "../reducers/app.reducer";

export function openModal({ modal, config }) {
  return (dispatch) => {
    dispatch({
      type: OPEN_MODAL,
      payload: { modal, config },
    });
  };
}

export function closeModal() {
  return (dispatch) => {
    dispatch({
      type: CLOSE_MODAL,
    });
  };
}
