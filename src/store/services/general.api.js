import { appInterceptedAxios } from "./interceptors";

export function chcekout({ customer, ads }) {
  const adIds = ads.map((ad) => ({ adId: ad.ad, quantity: ad.quantity }));
  return appInterceptedAxios({
    method: "post",
    url: "/checkout",
    data: { customer, adIds },
    loadKey: "Calculating Price",
    isInterruptive: true,
    successPrompt: {
      header: "Success",
      message: "Calculate Success",
    },
  });
}

export function listCustomers() {
  return appInterceptedAxios({
    method: "get",
    url: "/list/customer",
    isInterruptive: true,
  });
}

export function listAdType() {
  return appInterceptedAxios({
    method: "get",
    url: "/list/ads",
    isInterruptive: true,
  });
}
