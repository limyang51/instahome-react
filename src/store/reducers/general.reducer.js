export const SET_LIST_CUSTOMERS = "[General] Set List Of Customer";
export const SET_LIST_ADS = "[General] Set List of Ads";
export const SET_CHECKOUT_BILL = "[General] Set Checkout Bill";
export const ADD_NEW_ITEM_CART = "[General] Add New Item Cart";
export const APPEND_ITEM_CART = "[General] Append Item Cart";

const initialState = {
  customers: [],
  ads: [],
  bills: [],
};

export default function generalReducer(
  state = initialState,
  { payload, type }
) {
  switch (type) {
    case SET_LIST_CUSTOMERS:
      return {
        ...state,
        customers: ["Select Customer", ...payload.customers],
      };

    case SET_LIST_ADS:
      return {
        ...state,
        ads: [{ adType: "Select Type", price: 0 }, ...payload.adsType],
      };

    case ADD_NEW_ITEM_CART:
      const item = {
        customer: payload.item.customer,
        ads: [
          {
            ad: payload.item.ad,
            quantity: payload.item.quantity,
            price: payload.item.price,
          },
        ],
      };

      const newBills = [...state.bills, item];
      return {
        ...state,
        bills: newBills,
      };

    case APPEND_ITEM_CART:
      return { ...state, bills: [...payload.bills] };

    case SET_CHECKOUT_BILL:
      return {
        ...state,
        bills: [...payload.bills],
      };

    default:
      return state;
  }
}
