import { combineReducers } from "redux";
import appReducer from "./app.reducer";
import generalReducer from "./general.reducer";

export default combineReducers({
  app: appReducer,
  general: generalReducer,
});
