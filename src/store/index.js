// import { createBrowserHistory, createMemoryHistory } from "history";
// import { routerMiddleware } from "react-router-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import axios from "axios";
import rootReducer from "./reducers";

function configureStore(history, initialState = {}) {
  const enhancers = [];
  const middleware = [
    thunk.withExtraArgument(axios),
    // routerMiddleware(history),
  ];

  if (process.env.NODE_ENV === "development") {
    const devToolsExtension = window.devToolsExtension;

    if (typeof devToolsExtension === "function") {
      enhancers.push(devToolsExtension());
    }
  }

  const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
  );

  const store = createStore(rootReducer, initialState, composedEnhancers);

  return store;
}

// export const history =
//   process.env.NODE_ENV === "test"
//     ? createMemoryHistory()
//     : createBrowserHistory();

// export const store = configureStore(history);
export const store = configureStore();
