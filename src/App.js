import { StyleRoot } from "radium";
import LoadWrapper from "./component/LoadWrapper";
import InterruptiveLoadWrapper from "./component/InterruptiveLoadWrapper";
import ModalWrapper from "./component/ModalWrapper";
import FirstPage from "./page/first/first.page";
import "./App.scss";

function App() {
  return (
    <StyleRoot>
      <div className="scope-app">
        <ModalWrapper />
        <LoadWrapper />
        <InterruptiveLoadWrapper />
        <FirstPage />
      </div>
    </StyleRoot>
  );
}

export default App;
