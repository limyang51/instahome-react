import React from "react";

import "./Button.scss";

export const Button = (props) => {
  const {
    className = "",
    color,
    block,
    children,
    disabled,
    to,
    ...attr
  } = props;

  const renderClassName = () => {
    return `btn ${color ? `--${color}` : ""} ${className} ${
      block ? "--block" : ""
    }`.trim();
  };

  return (
    <button
      className={renderClassName()}
      type="button"
      disabled={disabled}
      {...attr}
    >
      {children}
    </button>
  );
};
