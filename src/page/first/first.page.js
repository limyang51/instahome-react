import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Page, Select, Flex, Input, Form, Submit } from "../../framework";
import {
  listCustomer,
  listAds,
  addItem,
  calculatePrice,
} from "../../store/actions/general.action";
import CheckoutTable from "../../component/CheckoutTable";

const INPUT_CUSTOMER = "customer";
const INPUT_AD = "ad";
const INPUT_QUANTITY = "quantity";

class FirstPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      [INPUT_CUSTOMER]: "",
      [INPUT_AD]: "",
      [INPUT_QUANTITY]: "",
    };
  }

  componentDidMount() {
    this.props.listCustomer();
    this.props.listAds();
  }

  handleInputChange(state, event) {
    this.setState({ [state]: event.target.value });
  }

  addItem() {
    this.props.addItem(this.state);
  }

  calculatePrice(bill) {
    this.props.calculatePrice(bill);
  }

  render() {
    return (
      <Page paperWhite fluid>
        <Form onSubmit={this.addItem.bind(this)}>
          <Flex>
            <Select
              label="Customer"
              value={this.state[INPUT_CUSTOMER]}
              onChange={this.handleInputChange.bind(this, INPUT_CUSTOMER)}
              inline
            >
              {(this.props.customers || []).map((name, idx) => (
                <option key={idx} value={name}>
                  {name}
                </option>
              ))}
            </Select>

            <Select
              label="Ads"
              value={this.state[INPUT_AD]}
              onChange={this.handleInputChange.bind(this, INPUT_AD)}
              inline
            >
              {(this.props.ads || []).map((ad, idx) => (
                <option key={idx} value={ad.adType}>
                  {ad.adType}
                </option>
              ))}
            </Select>

            <Input
              label="Quantity"
              type="number"
              step="1"
              onChange={this.handleInputChange.bind(this, INPUT_QUANTITY)}
            />

            <Submit color="primary">Add</Submit>
          </Flex>
        </Form>

        <CheckoutTable
          bills={this.props.bills}
          onCalculate={(bill) => {
            this.calculatePrice(bill);
          }}
        />
      </Page>
    );
  }
}

function mapStateToProps({ general: { customers, ads, bills } }) {
  return {
    customers,
    ads,
    bills,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { listCustomer, listAds, addItem, calculatePrice },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(FirstPage);
